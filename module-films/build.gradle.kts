plugins {
    kotlin("jvm")
    id("maven-publish")
    id("io.spring.dependency-management")
}

dependencyManagement {
    imports {
        val springVersion: String by project
        mavenBom("org.springframework.boot:spring-boot-dependencies:$springVersion")
    }
}

group = rootProject.group
version = rootProject.version

dependencies {
    implementation(kotlin("stdlib"))

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    val exposedStarterVersion: String by project
    implementation("org.jetbrains.exposed:exposed-spring-boot-starter:$exposedStarterVersion")

    api("org.example.tmg:db-schema:0.1.0")
    api("org.example.tmg:common:0.1.0")
}


publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["kotlin"])
        }
    }
}

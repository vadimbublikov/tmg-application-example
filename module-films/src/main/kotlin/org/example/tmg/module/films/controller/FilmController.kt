package org.example.tmg.module.films.controller

import org.example.tmg.module.common.PlayerCountFilms
import org.example.tmg.module.common.PlayersStatistic
import org.example.tmg.module.db.schema.FilmTable
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/film")
class FilmController(val playersStatistic: PlayersStatistic) {

    @GetMapping("/list")
    fun list(): List<FilmDto> = transaction {
            FilmTable.selectAll().map {
                FilmDto(
                    sequelNum = it[FilmTable.sequelNum],
                    name = it[FilmTable.name],
                    director = it[FilmTable.director]
                )
        }
    }

    @GetMapping("/playersOnFilms")
    fun playersOnFilms(): List<PlayerCountFilms> {
        return playersStatistic.playersFilmsList()
    }

}

package org.example.tmg.module.films.controller

data class FilmDto (
    val sequelNum: Int,
    val name: String,
    val director: String,
)

package org.example.tmg.app.config.db

import org.example.tmg.module.db.schema.FilmTable
import org.example.tmg.module.db.schema.PlayerTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct
import javax.sql.DataSource

@Configuration
class CreateData(val ds: DataSource) {

    @PostConstruct

    fun postConstruct() {
        Database.connect(ds)

        transaction {
            SchemaUtils.create(FilmTable, PlayerTable)
        }

        transaction {
            addLogger(StdOutSqlLogger)
            val filmLastJedi = FilmTable.insertAndGetId {
                it[name] = "The Last Jedi"
                it[sequelNum] = 8
                it[director] = "Rian Johnson"
            }
            val filmForceAwakens = FilmTable.insertAndGetId {
                it[name] = "The Force Awakens"
                it[sequelNum] = 7
                it[director] = "J.J. Abrams"
            }
            val filmTerminator = FilmTable.insertAndGetId {
                it[name] = "Терминатор 2: судный день"
                it[sequelNum] = 2
                it[director] = "Джеймс Кэмерон"
            }

            PlayerTable.insert {
                it[name] = "Арнольд Шварценеггер"
                it[filmId] = filmTerminator
            }
            PlayerTable.insert {
                it[name] = "Daisy Ridley"
                it[filmId] = filmLastJedi
            }
            PlayerTable.insert {
                it[name] = "Daisy Ridley"
                it[filmId] = filmForceAwakens
            }

        }
    }




}

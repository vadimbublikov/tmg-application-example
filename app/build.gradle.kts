plugins {
    kotlin("jvm")
    application

    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("plugin.spring")


}

group = rootProject.group
version = rootProject.version

dependencies {
    api("org.example.tmg:db-schema:0.1.0")
    api("org.example.tmg:common:0.1.0")
    api("org.example.tmg:module-films:0.1.0")
    api("org.example.tmg:module-players:0.1.0")

    implementation(kotlin("stdlib"))

    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    val exposedStarterVersion: String by project
    implementation("org.jetbrains.exposed:exposed-spring-boot-starter:$exposedStarterVersion")

    runtimeOnly("com.h2database:h2")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter")

}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

application {
    mainClass.set("org.example.tmg.app.TmgApplicationKt")
}





package org.example.tmg.module.db.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object FilmTable : IntIdTable() {
    val sequelNum = integer("sequel_num").uniqueIndex()
    val name = varchar("name", 50)
    val director = varchar("director", 50)
}

object PlayerTable : IntIdTable() {
    val name = varchar("name", 50)
    val filmId = reference("film_id", FilmTable)
}

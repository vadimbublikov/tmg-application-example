plugins {
    kotlin("jvm")
    id("maven-publish")
}

group = rootProject.group
version = rootProject.version

dependencies {
    implementation(kotlin("stdlib"))

    val exposedStarterVersion: String by project
    implementation("org.jetbrains.exposed:exposed-spring-boot-starter:$exposedStarterVersion")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["kotlin"])
        }
    }
}

rootProject.name = "tmg-application-example"

pluginManagement {
    plugins {
        val kotlinVersion: String by settings
        kotlin("jvm") version kotlinVersion apply false

        val springVersion: String by settings
        val springDependencyManagementVersion: String by settings
        val springPluginVersion: String by settings
        id("org.springframework.boot") version springVersion apply false
        id("io.spring.dependency-management") version springDependencyManagementVersion apply false
        kotlin("plugin.spring") version springPluginVersion apply false
    }

    repositories {
        gradlePluginPortal()
    }
}


include(":app")
include(":module-films")
include(":module-players")
include(":db-schema")
include("common")

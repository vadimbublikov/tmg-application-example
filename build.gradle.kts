group = "org.example.tmg"
version = "0.1.0"

allprojects {
    repositories {
        mavenCentral()
        mavenLocal()
        jcenter()
    }


}

plugins {
    kotlin("jvm") apply false
    id("org.springframework.boot") apply false
    kotlin("plugin.spring") apply false
}


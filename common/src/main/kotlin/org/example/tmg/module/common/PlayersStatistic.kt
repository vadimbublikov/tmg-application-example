package org.example.tmg.module.common


interface PlayersStatistic {
    fun playersFilmsList(): List<PlayerCountFilms>
}

data class PlayerCountFilms (
    val name: String,
    val count: Int
)

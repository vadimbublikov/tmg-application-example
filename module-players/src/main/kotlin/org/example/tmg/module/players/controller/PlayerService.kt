package org.example.tmg.module.players.controller

import org.example.tmg.module.common.PlayerCountFilms
import org.example.tmg.module.common.PlayersStatistic
import org.example.tmg.module.db.schema.FilmTable
import org.example.tmg.module.db.schema.PlayerTable
import org.jetbrains.exposed.sql.count
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.stereotype.Service

@Service
class PlayerService : PlayersStatistic {
    override fun playersFilmsList(): List<PlayerCountFilms> = transaction {
        ((PlayerTable innerJoin FilmTable)
            .slice(PlayerTable.name, FilmTable.id.count())
            .selectAll()
            .groupBy(PlayerTable.name))
            .map {
                PlayerCountFilms(it[PlayerTable.name], it[FilmTable.id.count()].toInt())
            }
    }
}



package org.example.tmg.module.players.controller

import org.example.tmg.module.db.schema.FilmTable
import org.example.tmg.module.db.schema.PlayerTable
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/player")
class PlayerController {

    @GetMapping("/list")
    fun list(): List<PlayerDto> = transaction {
        (PlayerTable innerJoin FilmTable).selectAll().map {
            PlayerDto(
                name = it[PlayerTable.name],
                filmName = it[FilmTable.name]
            )
        }
    }
}

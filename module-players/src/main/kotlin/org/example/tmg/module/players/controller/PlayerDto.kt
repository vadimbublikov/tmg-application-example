package org.example.tmg.module.players.controller

data class PlayerDto (
    val name: String,
    val filmName: String,
)
